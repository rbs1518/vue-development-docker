## Docker based Vue development environment.

### How-To

* Create a directory for your project.
* Go to your project directory with `cd <your_project_directory>`.
* Inside, create directory named `src`.
* Make sure `src` directory is writable with `chmod -R 0777 src`.
* Clone this repository into current directory.
* Initialize project files with `./init-project.sh`.
* Copy env example with `cp env-example .env`.
* It is important to modify VSCODE_TOKEN with your unique token. You can modify other variables as needed.
* Copy docker compose example with `cp docker-compose.example docker-compose.yml`.
* If remote vscode is not required, you can safely delete it from docker-compose.yml.
* If everything is ok, then run `docker-compose up -d` to start your project environment.

To access your app, visit http://<server_or_localhost>:<APP_PORT>

And for vscode, visit http://<server_or_localhost>:<VSCODE_PORT>?tkn=<VSCODE_TOKEN>


By default app port is 8082, and vscode port is 3002.
